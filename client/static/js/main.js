Vue.filter('formatDate', function (value) {
    var date = new Date();

    date.setTime(value);

    var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

    return hours + ':' + minutes + ':' + seconds;
});

new Vue({
    el: '#wrap',
    data: {
        chatHeight: 100,
        currentMessage: '',
        history: [],
        connection: null,
        chatHistoryNode: null,
        scrollEnd: true,
        myId: 0,
        notificationAudio: null,
        countUsers: 0,
        error: false
    },
    mounted: function () {
        this.chatHistoryNode = this.$el.querySelector('#chat-history');
        this.$nextTick(function () {
            window.addEventListener('resize', this.windowResizeHandle);
            this.chatHistoryNode.addEventListener('scroll', this.scrollHandle);
            this.windowResizeHandle()
        });


        this.chatHistoryNode = this.$el.querySelector('#chat-history');
        this.notificationAudio = new Audio('static/assets/notification.ogg');

        this.connection = new WebSocket('ws://' + location.host + '/ws');
        this.connection.onmessage = this.readSocket;
        this.connection.onclose = this.socketClosed;
        this.connection.onerror = this.socketClosed;
        this.scrollToEnd();
    },

    updated: function () {
        if (this.scrollEnd) {
            this.scrollToEnd();
        }
    },

    methods: {
        windowResizeHandle: function () {
            this.chatHeight = document.documentElement.clientHeight - 200;
        },

        scrollHandle: function() {
            this.scrollEnd = this.chatHistoryNode.scrollTop + this.chatHistoryNode.offsetHeight === this.chatHistoryNode.scrollHeight;
        },

        scrollToEnd: function() {
            this.chatHistoryNode.scrollTop = this.chatHistoryNode.scrollHeight;
        },
        newLine: function() {
            this.currentMessage += '\n';
        },
        sendMessage: function (event) {
            if (!event.ctrlKey && this.currentMessage.length > 0) {
                this.connection.send(JSON.stringify({method: 'send', data: this.currentMessage}));
                this.currentMessage = '';
            }
        },

        readSocket: function (data) {
            var response = JSON.parse(data.data);
            switch (response.method) {
                case 'new_message':
                    this.history.push(response.data);
                    if (response.data.author_id !== this.myId) {
                        this.notificationAudio.play();
                    }
                    break;
                case 'init_info':
                    if (response.data.history) {
                        this.history = response.data.history;
                    }
                    this.myId = response.data.my_id;
                    this.countUsers = response.data.count_users;
                    break;
                case 'online':
                    this.countUsers = response.data;
                    break
            }
        },
        socketClosed: function (event) {
            this.error = true;
        }
    }

});