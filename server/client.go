package main

import (
	"golang.org/x/net/websocket"
	"encoding/json"
	"io"
	"log"
	"time"
	"strings"
)

type Client struct {
	Id uint32 `json:"id"`
	Nickname string `json:"nickname"`
	conn *websocket.Conn `json:"-"`
	isOpenConnection bool `json:"-"`
	server *Server `json:"-"`
	Hash string `json:"-"`
	WriteMsg chan *Message `json:"-"`
	WritePacket chan *Packet `json:"-"`
}

func NewClient(s *Server, hash string) *Client {
	return &Client{
		WriteMsg:make(chan *Message),
		server:s,
		Hash:hash,
		WritePacket:make(chan *Packet),
	}
}

func (client *Client) Listen() {
	go client.listenWrite()
	client.listenRead();
}

func (client *Client) listenWrite() {
	for {
		select {
		case packet := <- client.WritePacket:
			client.sendPacket(packet)
		}
	}
}

func (client *Client) listenRead() {
	var request Packet
	for {

		err := websocket.JSON.Receive(client.conn, &request)

		if err == io.EOF {
			client.server.DelClient(client)
			return
		}

		switch request.Method {
		case "send":
			client.receiveSend(request)
		}
	}
}

func (client *Client) receiveSend(packet Packet) {
	var msgStr string
	if packet.Data == nil {
		return
	}
	
	err := json.Unmarshal([]byte(*packet.Data), &msgStr)
	msgStr = strings.TrimSpace(msgStr)
	lenMsg := len(msgStr)
	if lenMsg > 400 || lenMsg <= 0 {
		return
	}


	if err != nil {
		return
	}

	msg := &Message{AuthorId:client.Id, Author:client.Nickname, Text:msgStr, Time:time.Now().Unix() * 1000}

	client.server.NewMsg <- msg
	log.Println("Новое сообщение:", msgStr)
}

func (client *Client) sendPacket(packet *Packet) {
	websocket.JSON.Send(client.conn, packet)
}