package main

import (
	"net/http"
	"log"
	"os"
	"path/filepath"
	"time"
)

type Message struct {
	AuthorId uint32 `json:"author_id"`
	Author string `json:"author"`
	Text string `json:"text"`
	Time int64 `json:"time"`
}

var SERVER_ROOT string

func clearHistory(ticker *time.Ticker, server *Server)  {
	for {
		select {
		case <- ticker.C:
			server.ClearHistory <- true
		}
	}
}

func main()  {
	SERVER_ROOT = filepath.Dir(os.Args[0])

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(filepath.Join(filepath.Dir(os.Args[0]), "public/static/")))))
	server := NewServer()

	ticker := time.NewTicker(5 * time.Minute)

	go clearHistory(ticker, server)

	go server.Listen()

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
