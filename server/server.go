package main

import (
	"net/http"
	"golang.org/x/net/websocket"
	"log"
	"path/filepath"
	"time"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
)

type Server struct {
	Clients map[string]*Client
	UsersOnline map[uint32]*Client
	History []*Message
	CurrentID uint32
	NewMsg chan *Message
	SendOnline chan bool
	ClearHistory chan bool
}

type Packet struct {
	Method string `json:"method"`
	Data *json.RawMessage `json:"data"`
}

type InitData struct {
	History *[]*Message `json:"history"`
	CountUsers int `json:"count_users"`
	MyId uint32 `json:"my_id"`
}

func NewServer() *Server {
	return &Server{
		Clients:make(map[string]*Client),
		CurrentID:0,
		NewMsg:make(chan *Message),
		UsersOnline:make(map[uint32]*Client),
		ClearHistory:make(chan bool),
		SendOnline:make(chan bool),
	}
}

func (server *Server) AddClient(client *Client) {
	client.Id = server.CurrentID + 1
	server.CurrentID = client.Id

	server.Clients[client.Hash] = client
	server.UsersOnline[client.Id] = client
}

func (server *Server) DelClient(client *Client)  {
	client.conn.Close()
	client.isOpenConnection = false;
	delete(server.UsersOnline, client.Id)
	log.Println("Отключен ID:", client.Id)
}

func (server *Server) clearHistory() {
	lenHistory := len(server.History)
	log.Println("Количество сообщений на данный момент:", lenHistory)
	if lenHistory > 100 {
		server.History = server.History[99:]
		log.Println("Отчистка сообщений...")
	}
}

func (server *Server) handleHome(w http.ResponseWriter, r *http.Request) {
	ssid, errCookie := r.Cookie("chatssid")
	if errCookie != nil {
		http.Redirect(w, r,  "/auth", 302)
		return
	}

	if _, ok := server.Clients[ssid.Value]; ok {
		http.ServeFile(w, r, filepath.Join(SERVER_ROOT, "public/index.html"))
		return
	}

	http.Redirect(w, r,  "/auth", 302)
}

func (server *Server) handleAuth(w http.ResponseWriter, r *http.Request)  {
	ssid, errCookie := r.Cookie("chatssid")

	if errCookie == nil {
		if _, ok := server.Clients[ssid.Value]; ok {
			http.Redirect(w, r, "/", 302)
		}
	}

	nickname := r.FormValue("nickname");

	if nickname == "" {
		http.ServeFile(w, r, filepath.Join(SERVER_ROOT, "public/auth.html"))
		return
	}

	if len(nickname) > 20 {
		http.ServeFile(w, r, filepath.Join(SERVER_ROOT, "public/auth.html"))
		return
	}

	timeBytes, _ := time.Now().MarshalBinary()

	hasher := sha1.New()
	hasher.Write([]byte(nickname))
	hasher.Write(timeBytes)

	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	client := NewClient(server, sha)
	client.Nickname = nickname

	server.AddClient(client)

	cookie := http.Cookie{
		Name:"chatssid",
		Value:sha,
	}

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/", 302)
}

func (server *Server) sendInitInfo(ws *websocket.Conn, userId uint32)  {
	lenHistory := len(server.History)

	var start int

	if lenHistory <= 30 {
		start = 0
	} else {
		start = lenHistory - 30
	}
	history := server.History[start:lenHistory]
	initInfo := InitData{
		History:&history,
		CountUsers:len(server.UsersOnline),
		MyId:userId,
	}

	marshalInfo, _ := json.Marshal(&initInfo)
	raw := json.RawMessage(marshalInfo)
	packet := Packet{
		Method:"init_info",
		Data:&raw,
	}

	websocket.JSON.Send(ws, packet)
}

func (server *Server) sendOnline() {
	onlineJson, _ := json.Marshal(len(server.UsersOnline))
	online := json.RawMessage(onlineJson)
	packet := Packet{
		Method:"online",
		Data:&online,
	}

	for _, client := range server.UsersOnline {
		client.WritePacket <- &packet
	}
}

func (server *Server) sendMsg(msg *Message)  {
	server.History = append(server.History, msg)

	marshalMsg, _  := json.Marshal(msg)
	response := Packet{
		Method: "new_message",
	}

	raw := json.RawMessage(marshalMsg)

	response.Data = &raw

	for _, client := range server.Clients {
		client.WritePacket <- &response
	}
}

func (server *Server) tickerSendOnline()  {
	ticker := time.NewTicker(30 * time.Second)

	for {
		select {
		case <- ticker.C:
			server.SendOnline <- true
		}
	}
}

func (server *Server) Listen() {
	http.HandleFunc("/", server.handleHome)
	http.HandleFunc("/auth", server.handleAuth)
	http.Handle("/ws", websocket.Handler(func(ws *websocket.Conn) {
		ssidCookie, err := ws.Request().Cookie("chatssid")
		if err != nil {
			log.Println("Подключение без куки")
			return
		}
		client, ok := server.Clients[ssidCookie.Value]
		if !ok {
			log.Println("Ошибка авторизации")
			return
		}
		if client.isOpenConnection {
			ws.Close()
			return
		}
		client.conn = ws
		client.isOpenConnection = true
		server.UsersOnline[client.Id] = client
		server.sendInitInfo(ws, client.Id)
		log.Println("Новое подключение ID:", client.Id)
		client.Listen()
	}));


	go server.tickerSendOnline()


	for {
		select {
		case msg := <- server.NewMsg:
			server.sendMsg(msg)
		case <- server.SendOnline:
			server.sendOnline()
		case <- server.ClearHistory:
			server.clearHistory()
		}
	}
}